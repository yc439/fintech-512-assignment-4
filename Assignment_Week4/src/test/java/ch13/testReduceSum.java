package ch13;

import static org.junit.Assert.*;

import org.junit.Test;

public class testReduceSum {

	@Test
	public void test_ReduceSum() {
		Expression sum= new Sum(Money.dollar(3), Money.dollar(4));
		Bank bank= new Bank();
		Money result= bank.reduce(sum, "USD");
		assertEquals(Money.dollar(7), result);
	}

}
