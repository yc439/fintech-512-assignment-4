package ch3;

import static org.junit.Assert.*;

import org.junit.Test;

public class testEquality {

	@Test
	public void test_Equality() {
		assertTrue(new Dollar(5).equals(new Dollar(5)));
		assertFalse(new Dollar(5).equals(new Dollar(6)));
	}

}
