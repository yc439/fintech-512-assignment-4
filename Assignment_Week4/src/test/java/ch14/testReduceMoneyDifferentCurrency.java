package ch14;

import static org.junit.Assert.*;

import org.junit.Test;

public class testReduceMoneyDifferentCurrency {

	@Test
	public void test_ReduceMoneyDifferentCurrency() {
		Bank bank= new Bank();
		bank.addRate("CHF", "USD", 2);
		Money result= bank.reduce(Money.franc(2), "USD");
		assertEquals(Money.dollar(1), result);
	}
	

}
