package ch14;

import static org.junit.Assert.*;

import org.junit.Test;

public class testIdentityRate {

	@Test
	public void test_IdentityRate() {
		assertEquals(1, new Bank().rate("USD", "USD"));
	}

}
