package ch4;

import static org.junit.Assert.*;

import org.junit.Test;

public class testMultiplication {

	@Test
	public void test_Multiplication() {
		Dollar five= new Dollar(5);
		assertEquals(new Dollar(10), five.times(2));
		assertEquals(new Dollar(15), five.times(3));
	}

}
