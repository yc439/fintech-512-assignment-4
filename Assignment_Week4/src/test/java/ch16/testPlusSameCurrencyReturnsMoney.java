package ch16;

import static org.junit.Assert.*;

import org.junit.Test;

public class testPlusSameCurrencyReturnsMoney {

	@Test
	public void test_PlusSameCurrencyReturnsMoney() {
		Expression sum= Money.dollar(1).plus(Money.dollar(1));
		assertTrue(sum instanceof Money);
	}
	

}
