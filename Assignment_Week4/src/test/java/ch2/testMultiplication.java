package ch2;

import static org.junit.Assert.*;

import org.junit.Test;

public class testMultiplication {

	@Test
	public void test_Multiplication() {
		Dollar five= new Dollar(5);
		Dollar product= five.times(2);
		assertEquals(10, product.amount);
		product= five.times(3);
		assertEquals(15, product.amount);
	}
}
