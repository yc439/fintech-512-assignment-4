package ch1;

import static org.junit.Assert.*;

import org.junit.Test;

public class testMultiplication {

	@Test
	public void test_Multiplication() {
		Dollar five= new Dollar(5);
		five.times(2);
		assertEquals(10, five.amount);
	}

}
