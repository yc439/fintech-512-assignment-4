package ch9;

public class Franc extends Money {
	public Franc(int i,String currency) {
		super(i,currency);
	}
	Money times(int multiplier) {
		return Money.franc(amount * multiplier);
	}
	String currency() {
		return currency;
	}
}
