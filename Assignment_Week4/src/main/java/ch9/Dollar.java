package ch9;

public class Dollar extends Money {
	public Dollar(int i,String currency ) {
		super(i, currency);
	}
	Money times(int multiplier) {
		return Money.dollar(amount * multiplier);
	}
	String currency() {
		return currency;
	}
}
