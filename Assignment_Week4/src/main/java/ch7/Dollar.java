package ch7;


public class Dollar extends Money{
	public Dollar(int i) {
		amount = i;
	}
	Dollar times(int multiplier) {
		return new Dollar(amount * multiplier);
	}

}