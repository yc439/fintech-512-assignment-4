package ch7;


public class Franc extends Money{
	public Franc(int i) {
		amount = i;
	}
	Franc times(int multiplier) {
		return new Franc(amount * multiplier);
	}
}
