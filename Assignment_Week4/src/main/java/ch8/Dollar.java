package ch8;

public class Dollar extends Money{
	public Dollar(int i) {
		amount = i;
	}
	Money times(int multiplier) {
		return new Dollar(amount * multiplier);
	}
}
