package ch8;


public class Franc extends Money{
	public Franc(int i) {
		amount = i;
	}
	Money times(int multiplier) {
		return new Franc(amount * multiplier);
	}
}
